package com.fall.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "categories")
@Embeddable
public class CategoryModel {

    @Id
    private String categoryId;
    private String name;
    private String imageURL;
    private String categoryName;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "category_product",
            joinColumns = {@JoinColumn(name = "category_Id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id")}
    )
    private Set<ProductModel> products;
    @ManyToMany(mappedBy = "categories", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<UserModel> users;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Set<ProductModel> getProducts() {
        return products;
    }

    public Set<UserModel> getUsers() {
        return users;
    }

    public void setUsers(Set<UserModel> users) {
        this.users = users;
    }

    public void setProducts(Set<ProductModel> products) {
        this.products = products;
    }


}
