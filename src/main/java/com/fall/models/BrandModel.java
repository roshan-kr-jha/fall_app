package com.fall.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "brands")
@Embeddable
public class BrandModel {

    private String name;
    @Id
    private String code;
    private String imageURL;
    @ManyToMany(mappedBy = "brands", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Set<UserModel> users;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Set<UserModel> getUsers() {
        return users;
    }

    public void setUsers(Set<UserModel> users) {
        this.users = users;
    }
}
