package com.fall.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;

@Configuration
public class EmailConfig {

    @Bean
    public SimpleMailMessage emailTemplate()
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("prinil1420@gmail.com");
        message.setFrom("support@fall.com");
        message.setSubject("Email Triggerred from Fall App System");
        message.setText("FATAL - Application crash. Save your job !!");
        return message;
    }
}
