package com.fall.controllers.cart;

import com.fall.dtos.CartData;
import com.fall.dtos.UserData;
import org.springframework.web.bind.annotation.*;

@RestController
public class CartPageController {

    @GetMapping("/cart")
    private CartData getCart(){
        return new CartData();
    }

    @PutMapping("/updateQty/{entryNo}/{qty}")
    private CartData update(@PathVariable int entryNo, @PathVariable int qty) {
        return new CartData();
    }

    @DeleteMapping("/delete/{entryNo}")
    private boolean delete(@PathVariable String entryNo) {
        return false;
    }

    @DeleteMapping("/delete")
    private boolean delete() {
        return false;
    }

    @PostMapping("/add")
    private CartData login(@RequestBody UserData user) {
        return new CartData();
    }


}
