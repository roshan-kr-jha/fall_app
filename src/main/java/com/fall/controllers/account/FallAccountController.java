package com.fall.controllers.account;

import com.fall.dtos.*;
import com.fall.models.BrandModel;
import com.fall.models.CategoryModel;
import com.fall.models.UserModel;
import com.fall.service.brands.FallBrandService;
import com.fall.service.categories.FallCategoryService;
import com.fall.service.email.FallEmailService;
import com.fall.service.profile.FallAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class FallAccountController {

    private static Logger LOG = LoggerFactory.getLogger(FallAccountController.class);

    @Autowired
    private FallAccountService fallAccountService;

    @Autowired
    private FallBrandService fallBrandService;

    @Autowired
    private FallCategoryService fallCategoryService;

    @Autowired
    private FallEmailService fallEmailService;

    @GetMapping("/")
    private String testApp(){

       // fallEmailService.sendMail("roshankumarjha24@gmail.com", "Hi", "Ho ho ho");
      //  fallEmailService.sendPreConfiguredMail("Welcome to Fall App");

        fallAccountService.storeBrandsInfo();
        fallAccountService.storeCategoryInfo();
        return "SUCCESS";
    }

    @PostMapping("/register")
    private RegistrationData register(@RequestBody UserData newUser) {
        String status = fallAccountService.registerNewUser(newUser);
        RegistrationData registrationData = new RegistrationData();
        if(status.contains("USER_ALREADY_EXISTS") || status.contains("USER_CREATED")){
            registrationData.setUserId(status.split(" ")[1]);
            populateBrandsData(registrationData);
            populateCategoryData(registrationData);
            registrationData.setStatus(status.split(" ")[0]);
        }else {
            registrationData.setStatus(status);
        }
        return registrationData;
    }

    @GetMapping("/brands")
    private RegistrationData getBrands() {
        RegistrationData registrationData = new RegistrationData();
        populateBrandsData(registrationData);
        return registrationData;
    }

    private void populateBrandsData(RegistrationData registrationData) {
       List<BrandModel> brands = fallBrandService.getAllBrands();
       List<BrandData> brandDataList = new ArrayList<>();
        for (BrandModel br:brands) {
            BrandData brandData = new BrandData();
            brandData.setCode(br.getCode());
            brandData.setName(br.getName());
            brandData.setImageURL(br.getImageURL());
            brandDataList.add(brandData);
        }
        registrationData.setBrands(brandDataList);
    }

    @GetMapping("/categories")
    private RegistrationData getCategories() {
        RegistrationData registrationData = new RegistrationData();
        populateCategoryData(registrationData);
        return registrationData;
    }

    private void populateCategoryData(RegistrationData registrationData) {

        List<CategoryModel> cats = fallCategoryService.getAllCategories();
        List<CategoryData> categoryDataList = new ArrayList<>();
        for (CategoryModel br:cats) {
            CategoryData catData = new CategoryData();
            catData.setCode(br.getCategoryId());
            catData.setName(br.getName());
            catData.setImageURL(br.getImageURL());
            categoryDataList.add(catData);
        }
        registrationData.setCategories(categoryDataList);
    }


    @PostMapping("/login")
    private StatusData login(@RequestBody UserData user) {
        boolean res =  fallAccountService.validateUserForLogin(user.getEmail(),user.getPassword());
        StatusData loginResponse = new StatusData();
        loginResponse.setSuccessful(res);
        return loginResponse;
    }

    @PutMapping("/update")
    private StatusData update(@RequestBody UserData user) {
        boolean res =  fallAccountService.updateAccount(user);
        StatusData statusData = new StatusData();
        statusData.setSuccessful(res);
        return statusData;
    }

    @PostMapping("/changePassword")
    private StatusData changePassword(@RequestBody UserData userData){
        final UserModel user = fallAccountService.getUserByEmail(userData.getEmail());
        boolean res = false;
        if(null != user){
            res = fallAccountService.updateAccount(userData);
        }
        final StatusData statusData = new StatusData();
        statusData.setSuccessful(res);
        return statusData;
    }

    @DeleteMapping("/delete/{userId}")
    private boolean delete(@PathVariable String userId) {
        return fallAccountService.deleteUser(Long.valueOf(userId));
    }

    @GetMapping("/fetch/{userId}")
    private UserData getUserById(@PathVariable String userId){
        UserModel user = fallAccountService.getUserById(Long.valueOf(userId));
        UserData userData = new UserData();
        userData.setUserId(user.getUserId());
        userData.setAge(user.getAge());
        userData.setEmail(user.getEmail());
        userData.setPassword(user.getPassword());
        userData.setGender(user.getGender());
        userData.setMobileNo(user.getMobileNo());
        userData.setName(user.getName());
        userData.setUserId(user.getUserId());
        populateBrandsNCategoriesForUserData(user, userData);
        return userData;
    }

    private void populateBrandsNCategoriesForUserData(UserModel user, UserData userData) {
        List<BrandData> brandDataList = new ArrayList<>();
        List<CategoryData> categoryDataList = new ArrayList<>();
        for (BrandModel brand: user.getBrands()) {
            BrandData brandData = new BrandData();
            brandData.setName(brand.getName());
            brandData.setCode(brand.getCode());
            brandData.setImageURL(brand.getImageURL());
            brandDataList.add(brandData);
        }
        for (CategoryModel categoryModel: user.getCategories()) {
            CategoryData categoryData = new CategoryData();
            categoryData.setCode(categoryModel.getCategoryId());
            categoryData.setName(categoryModel.getName());
            categoryData.setImageURL(categoryModel.getImageURL());
            categoryDataList.add(categoryData);
        }
        userData.setBrands(brandDataList);
        userData.setCategories(categoryDataList);
    }

    @GetMapping("/all")
    private List<UserData> getAllUsers(){
        List<UserData> userList = new ArrayList<>();
        List<UserModel> users = fallAccountService.getAllUsers();
        for (final UserModel user:users) {
            UserData userData= new UserData();
            userData.setUserId(user.getUserId());
            userData.setAge(user.getAge());
            userData.setEmail(user.getEmail());
            userData.setPassword(user.getPassword());
            userData.setGender(user.getGender());
            userData.setMobileNo(user.getMobileNo());
            userData.setName(user.getName());
            userData.setUserId(user.getUserId());
            populateBrandsNCategoriesForUserData(user, userData);
            userList.add(userData);
        }
        return userList;
    }

}
