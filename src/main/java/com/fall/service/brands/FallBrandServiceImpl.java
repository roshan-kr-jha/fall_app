package com.fall.service.brands;

import com.fall.models.BrandModel;
import com.fall.repositories.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FallBrandServiceImpl implements FallBrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<BrandModel> getAllBrands() {
        return brandRepository.findAll();
    }
}
