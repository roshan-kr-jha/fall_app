package com.fall.service.brands;

import com.fall.models.BrandModel;

import java.util.List;

public interface FallBrandService {

    List<BrandModel> getAllBrands();
}
