package com.fall.service.profile.impl;

import com.fall.dtos.BrandData;
import com.fall.dtos.CategoryData;
import com.fall.dtos.UserData;
import com.fall.models.BrandModel;
import com.fall.models.CategoryModel;
import com.fall.models.UserModel;
import com.fall.repositories.BrandRepository;
import com.fall.repositories.CategoryRepository;
import com.fall.repositories.UserRepository;
import com.fall.service.profile.FallAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class FallAccountServiceImpl implements FallAccountService {
    private static Logger LOG = LoggerFactory.getLogger(FallAccountServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public String registerNewUser(final UserData user) {
        final UserModel newUser = userRepository.findByEmail(user.getEmail());
        if(null != newUser){
            return "USER_ALREADY_EXISTS " + newUser.getUserId();
        }
        try {
            UserModel userModel = new UserModel();
            userModel.setAge(user.getAge());
            userModel.setEmail(user.getEmail());
            userModel.setPassword(user.getPassword());
            userModel.setGender(user.getGender());
            userModel.setMobileNo(user.getMobileNo());
            userModel.setName(user.getName());
            Set<BrandModel> brands = new HashSet<>();
            for (BrandData brandData:user.getBrands()) {
                final BrandModel brand = brandRepository.findByCode(brandData.getCode());
                if(null != brand){
                    brands.add(brand);
                }
            }
            Set<CategoryModel> categoryModels = new HashSet<>();
            for (CategoryData categoryData:user.getCategories()) {
                final CategoryModel category = categoryRepository.findByCategoryId(categoryData.getCode());
                if(null != category) {
                    categoryModels.add(category);
                }
            }
            userModel.setBrands(brands);
            userModel.setCategories(categoryModels);
            userRepository.save(userModel);
            LOG.info("User with " + user.getEmail() + " is successfully registered.");
            UserModel registeredUser = userRepository.findByEmail(user.getEmail());
            return "USER_CREATED " + registeredUser.getUserId();
        }catch (final Exception e){
            LOG.error("User with userID " + user.getUserId() + " is not registered.", e);
        }
        return "USER_NOT_REGISTERED";
    }

    @Override
    public boolean validateUserForLogin(final String email,final String pass) {

        final UserModel user = userRepository.findByEmail(email);
        if(null != user){
            if(user.getPassword().equals(pass)){
                LOG.info("User with " + user.getUserId() + " is successfully logged In.");
                return true;
            }
        }
        LOG.error("Incorrect login details");
        return false;
    }

    @Override
    public boolean deleteUser(final Long userId) {
        try {
            userRepository.deleteById(userId);
            return true;
        }catch (final Exception e){
            LOG.error("User with " + userId + " is not deleted.", e);
        }
        return false;
    }

    @Override
    public boolean updateAccount(final UserData userData) {
        Optional<UserModel> user = userRepository.findById(userData.getUserId());
        if(user.isPresent()){
            UserModel userModel = new UserModel();
            userModel.setUserId(userData.getUserId());
            userModel.setAge(userData.getAge());
            userModel.setEmail(userData.getEmail());
            userModel.setPassword(userData.getPassword());
            userModel.setGender(userData.getGender());
            userModel.setMobileNo(userData.getMobileNo());
            userModel.setName(userData.getName());
            userModel.setUserId(user.get().getUserId());
            try {
                userRepository.save(userModel);
                LOG.info("Account details of " + userData.getUserId() + " is successfully updated.");
                return true;
            }catch (final Exception e){
                LOG.error("Account details of " + userData.getUserId() + " is not updated.", e);
            }
        }
        return false;
    }

    @Override
    public UserModel getUserById(final Long userID) {
        return userRepository.getById(userID);
    }

    @Override
    public UserModel getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<UserModel> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void storeBrandsInfo() {

            BrandModel brandModel1 = new BrandModel();
            brandModel1.setCode("armani");
            brandModel1.setName("ARMANI");
            brandModel1.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/07/Emporio-Armani-776x430.jpg");
            brandRepository.save(brandModel1);
            BrandModel brandModel2 = new BrandModel();
            brandModel2.setCode("fendi");
            brandModel2.setName("FENDI");
            brandModel2.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/9.FENDI_.jpg");
            brandRepository.save(brandModel2);
            BrandModel brandModel3 = new BrandModel();
            brandModel3.setCode("versace");
            brandModel3.setName("HOUSE OF VERSACE");
            brandModel3.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/8.-HOUSE-OF-VERSACE.jpg");
            brandRepository.save(brandModel3);
            BrandModel brandModel4 = new BrandModel();
            brandModel4.setCode("burberry");
            brandModel4.setName("BURBERRY");
            brandModel4.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/7.-BURBERRY.jpg");
            brandRepository.save(brandModel4);
            BrandModel brandModel5 = new BrandModel();
            brandModel5.setCode("ralph_lauren");
            brandModel5.setName("RALPH LAUREN");
            brandModel5.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/6.-RALPH-LAUREN.jpg");
            brandRepository.save(brandModel5);
            BrandModel brandModel6 = new BrandModel();
            brandModel6.setCode("chanel");
            brandModel6.setName("CHANEL");
            brandModel6.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/5.-CHANEL.jpg");
            brandRepository.save(brandModel6);
            BrandModel brandModel7 = new BrandModel();
            brandModel7.setCode("prada");
            brandModel7.setName("PRADA");
            brandModel7.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/4.-PRADA.jpg");
            brandRepository.save(brandModel7);
            BrandModel brandModel8 = new BrandModel();
            brandModel8.setCode("hermes");
            brandModel8.setName("HERMES");
            brandModel8.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/3.-HERMES.jpg");
            brandRepository.save(brandModel8);
            BrandModel brandModel9 = new BrandModel();
            brandModel9.setCode("gucci");
            brandModel9.setName("GUCCI");
            brandModel9.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/2.-GUCCI.jpg");
            brandRepository.save(brandModel9);
            BrandModel brandModel10 = new BrandModel();
            brandModel10.setCode("louis_vuitton");
            brandModel10.setName("LOUIS VUITTON");
            brandModel10.setImageURL("https://cordmagazine.com/wp-content/uploads/2017/08/1.-LOUIS-VUITTON.jpg");
            brandRepository.save(brandModel10);
    }

    @Override
    public void storeCategoryInfo() {

        CategoryModel categoryModel1 = new CategoryModel();
        categoryModel1.setCategoryId("casual");
        categoryModel1.setName("CASUAL DRESSING");
        categoryModel1.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/casual-e1505901412482.jpg");
        categoryRepository.save(categoryModel1);
        CategoryModel categoryModel2 = new CategoryModel();
        categoryModel2.setCategoryId("office");
        categoryModel2.setName("OFFICE WEAR");
        categoryModel2.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/office-e1505971098359.jpg");
        categoryRepository.save(categoryModel2);
        CategoryModel categoryModel3 = new CategoryModel();
        categoryModel3.setCategoryId("sports");
        categoryModel3.setName("SPORTS WEAR");
        categoryModel3.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/sportswear-e1505972192382.jpg");
        categoryRepository.save(categoryModel3);
        CategoryModel categoryModel4 = new CategoryModel();
        categoryModel4.setCategoryId("arty");
        categoryModel4.setName("ARTY");
        categoryModel4.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/arty-e1505893682672.jpg");
        categoryRepository.save(categoryModel4);
        CategoryModel categoryModel5 = new CategoryModel();
        categoryModel5.setCategoryId("classic");
        categoryModel5.setName("CLASSIC");
        categoryModel5.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/classic-e1505894121491.jpg");
        categoryRepository.save(categoryModel5);
        CategoryModel categoryModel6 = new CategoryModel();
        categoryModel6.setCategoryId("exotic");
        categoryModel6.setName("EXOTIC");
        categoryModel6.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/exotic-e1505894489717.jpg");
        categoryRepository.save(categoryModel6);
        CategoryModel categoryModel7 = new CategoryModel();
        categoryModel7.setCategoryId("street");
        categoryModel7.setName("STREET");
        categoryModel7.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/streetfashion-e1505890764149.jpg");
        categoryRepository.save(categoryModel7);
        CategoryModel categoryModel8 = new CategoryModel();
        categoryModel8.setCategoryId("preppy");
        categoryModel8.setName("PREPPY");
        categoryModel8.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/preppy-e1505886273295.jpg");
        categoryRepository.save(categoryModel8);
        CategoryModel categoryModel9 = new CategoryModel();
        categoryModel9.setCategoryId("vintage");
        categoryModel9.setName("VINTAGE");
        categoryModel9.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/vintage-e1505892264305.jpg");
        categoryRepository.save(categoryModel9);
        CategoryModel categoryModel10 = new CategoryModel();
        categoryModel10.setCategoryId("chic");
        categoryModel10.setName("CHIC");
        categoryModel10.setImageURL("https://www.urby.in/blog/wp-content/uploads/2017/09/chic-e1505892980209.jpg");
        categoryRepository.save(categoryModel10);
    }
}
