package com.fall.service.profile;

import com.fall.dtos.UserData;
import com.fall.models.UserModel;

import java.util.List;


public interface FallAccountService {

    String registerNewUser(final UserData user);
    boolean validateUserForLogin(final String email, final String pass);
    boolean deleteUser(final Long userID);
    boolean updateAccount(final UserData userData);
    UserModel getUserById(final Long userID);
    UserModel getUserByEmail(final String email);
    List<UserModel> getAllUsers();
    /*
    Its a temporary method to load brands & category info on Application startup.TO BE REMOVED WHEN PERMANENT DATABASE AVAILABLE.
     */
    void storeBrandsInfo();
    /*
     Its a temporary method to load brands & category info on Application startup.TO BE REMOVED WHEN PERMANENT DATABASE AVAILABLE.
   */
    void storeCategoryInfo();
}
