package com.fall.service.categories;

import com.fall.models.CategoryModel;

import java.util.List;

public interface FallCategoryService {

    List<CategoryModel> getAllCategories();
}
