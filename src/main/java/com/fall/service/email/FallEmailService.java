package com.fall.service.email;

public interface FallEmailService {

    void sendMail(String to, String subject, String body);
    void sendPreConfiguredMail(String message);
    void sendMailWithAttachment(String to, String subject, String body, String fileToAttach);
    void sendMailWithInlineResources(String to, String subject, String fileToAttach);
}
