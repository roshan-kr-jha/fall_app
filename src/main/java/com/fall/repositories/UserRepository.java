package com.fall.repositories;

import com.fall.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends JpaRepository<UserModel, Long> {
    UserModel findByEmail(final String email);
}
