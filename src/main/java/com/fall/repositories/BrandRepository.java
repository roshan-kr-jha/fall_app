package com.fall.repositories;

import com.fall.models.BrandModel;
import com.fall.models.CategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<BrandModel, Long> {
    BrandModel findByCode(final String code);
}
