package com.fall.repositories;

import com.fall.models.CategoryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<CategoryModel, Long> {
    CategoryModel findByCategoryId(final String catCode);
}
