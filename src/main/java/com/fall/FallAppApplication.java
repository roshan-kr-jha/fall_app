package com.fall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FallAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FallAppApplication.class, args);
	}

}
