package com.fall.dtos;

import java.util.List;

public class RegistrationData {

    private String userId;
    private String status;
    private List<BrandData> brands;
    private List<CategoryData> categories;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<BrandData> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandData> brands) {
        this.brands = brands;
    }

    public List<CategoryData> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryData> categories) {
        this.categories = categories;
    }
}
